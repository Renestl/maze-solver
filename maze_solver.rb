require 'set'
require_relative 'maze'

class MazeSolver
	def initialize(maze)
		@maze = maze
		@open_list = Set.new # nodes to be examined
		@closed_list = Set.new # nodes that make the path
		@start_node = @maze.find_start
		@end_node = @maze.find_end
		@current_node = nil
	end

	def add_start_node
		@open_list << @start_node
		@current_node = @start_node
		add_to_open
	end

	def find_next_node
		temp = nil

		@open_list.each do |node|
			if !@closed_list.include?(node)
				if temp == nil
					temp = estimated_full_cost(node)
				elsif estimated_full_cost(node) <= estimated_full_cost(temp)
					temp = node
				end
			end
		end

		@current_node = temp
		is_goal?(@current_node)
	end

	def add_to_open
		neighbors = Set.new

		@open_list.each do |node| 
			@maze.get_neighbors(node).each do|neighbor|
				neighbors << neighbor
			end		
		end
		
		@open_list = @open_list + neighbors

		add_to_closed(@current_node)
		find_next_node
	end

	def add_to_closed(node)
		score_path
		@closed_list << node
	end

	def score_path
		@maze.mark_path(@current_node)
	end

	def is_goal?(node)
		@end_position = @maze.find_end

		if  node == @end_position
			@maze.print_maze
			return true
		else
			add_to_open
			return false			
		end
	end

	# Node
	# Using Manhattan method
	def estimated_full_cost(node)
		estimated_full_cost = estimated_start_cost(node) + estimated_end_cost(node)
	end

	def estimated_start_cost(node)
		horizontal = (@start_node[0] - node[0]).abs
		vertical = (@start_node[1] - node[1]).abs

		estimated_start_cost = horizontal + vertical 
	end

	def estimated_end_cost(node)
		horizontal = (@end_node[0] - node[0]).abs
		vertical = (@end_node[1] - node[1]).abs

		estimated_end_cost = horizontal + vertical 
	end
end

if __FILE__ == $PROGRAM_NAME
	puts "Enter maze file name:"
	maze_solver = MazeSolver.new(Maze.new(gets.chomp))
	maze_solver.add_start_node
end