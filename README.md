# Maze Solver

Implementation in Ruby for [App Academy Open](https://open.appacademy.io/)

## Description

In this exercise, we want to write a program that will find a route between two points in a maze.

Here's an example maze. It has an 'S' for the start point, and an 'E' for an end point.

You should write a program that will read in the maze, try to explore a path through it to the end, and then print out a completed path like so. If there is no such path, it should inform the user.

Make your program run as a command line script, taking in the name of a maze file on the command line.

Your path through the maze should not be self-intersecting, of course.

When you have found a first solution, write a version which will be sure to find the shortest path through the maze.

## Resources

**NB:** Try taking a naive approach first. Once you've got something working read on...

* Reading Files [progzoo](http://progzoo.net/wiki/Ruby:Read_a_Text_File)
* Simple-ish explanation for computer pathfinding, start at "Starting the Search" heading [a-star](http://archive.gamedev.net/archive/reference/articles/article2003.html)
* Wikipedia: maze shortest path [wikipedia](http://en.wikipedia.org/wiki/Maze_solving_algorithm#Shortest_path_algorithm)

## To Play

1. Download the files or clone the repo to your desktop.
2. Open your terminal and navigate to the folder and run `ruby maze_solver.rb`
3. When prompted enter your maze filename. Include the file extension: `example_name.txt`