class Maze
	attr_reader :maze, :start, :end

	def initialize(filename)
		@maze = load_maze(filename)
		@start = find_start
		@end = find_end
	end

	def load_maze(filename)
		File.readlines(filename).map { |line| line.chomp.split("")}
	end

	def [](maze_position)
		row, col = maze_position
		@maze[row][col]
	end

	def []=(position, value)
		row, col = position
		@maze[row][col] = value
	end

	def is_wall?(position)
		return true if self[position] == "*"
		return false
	end

	def find_start
		find_exits("S")
	end

	def find_end
		find_exits("E")
	end

	def find_exits(exit)
		@maze.each_with_index do |row, col| 
			return [col, row.index(exit)] if row.index(exit)
		end
	end

	def mark_path(position)
		self[position] = "X"

		print_maze
	end

	def print_maze
		@maze.each do |row|
			puts row.join("")
		end
	end

	def get_neighbors(position)
		drow, dcol = position

		north = [drow - 1, dcol]
		south = [drow + 1, dcol]
		east = [drow, dcol + 1]
		west = [drow, dcol - 1]

		neighbors = []

		neighbors << north unless is_wall?(north) 
		neighbors << south unless is_wall?(south)
		neighbors << east unless is_wall?(east)
		neighbors << west unless is_wall?(west)
		
		neighbors
	end
end